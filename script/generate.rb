#!/usr/bin/env ruby

require 'nokogiri'
require 'pp'
require 'set'
require 'net/https'
require 'uri'
require 'json'
require 'digest'

require 'getoptlong'

opts = GetoptLong.new(
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
  [ '--test-images', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--test-type', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--name', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--add-headers', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--to-list', GetoptLong::OPTIONAL_ARGUMENT ],
)

action = 'none'

begin
  opts.each do |opt, arg|
    case opt
      when '--help'
        puts <<-EOF % [ File.basename($0) ]
%s [OPTION] ... DIR

      EOF
      exit
      when '--test-images'
        action = 'test-images'
      when '--test-type'
        action = 'test-type'
      when '--add-headers'
        action = 'add-headers'
      when '--to-list'
        action = 'to-list'
    end
  end
rescue GetoptLong::InvalidOption => x
  puts "error: %s " % [ x.class ]
  exit
end

def get_path(file_name)
    filename = file_name.gsub(' ', '_')
    filename = 'Nl-%s.ogg' % [ filename ]
    d = Digest::MD5.hexdigest filename
    filename = URI::encode(filename)
    return "https://upload.wikimedia.org/wikipedia/commons/%s/%s/%s" % [ d[0], d[0..1], filename ], "/%s/%s/%s" % [ d[0], d[0..1], filename ]
end

def find_type(uri)
  ret = nil
  Net::HTTP.start(uri.host, uri.port, :use_ssl => true ) { |http|
    response = http.get(uri.request_uri)
    doc = Nokogiri::XML(response.body)
    doc.css('.toctext').each do |span|
      type = span.children.first.text.strip
      [ 'Eigennaam',
        'Telbijwoord',
        'Tussenwerpsel',
        'Hoofdtelwoord',
        'Voorzetsel',
        'Aanwijzend voornaamwoord',
        'Onbepaald voornaamwoord',
        'Bezittelijk voornaamwoord',
        'Voornaamwoordelijk bijwoord',
        'Betrekkelijk voornaamwoord',
        'Bijvoeglijk naamwoord',
        'Voegwoord',
        'Persoonlijk voornaamwoord',
        'Bijwoord',
        'Werkwoord',
        'Zelfstandig naamwoord' ].each do |nw|
        if type == nw
#          puts "'%s' '%s'" % [ type, nw ]
          ret = nw
        end
      end
      break if (ret)
    end
  }
  ret
end

def test_types(doc)
  types = Hash[
    'aanw vn'    => 'Aanwijzend_voornaamwoord',
    'aanw vnw'   => 'Uitroepend_voornaamwoord',
    'betr vnw'   => 'Betrekkelijk_voornaamwoord',
    'bez vnw'    => 'Bezittelijk_voornaamwoord',
    'bn'         => 'Bijvoeglijk_naamwoord',
    'bw'         => 'Bijwoord',
    'de'         => 'Zelfstandig_naamwoord',
    'het, de'    => 'Zelfstandig_naamwoord',
    'het'        => 'Zelfstandig_naamwoord',
    'htw'        => 'Hoofdtelwoord',
    'onbep telw' => 'Onbepaald_hoofdtelwoord',
    'onbep vnw'  => 'Onbepaald_voornaamwoord',
    'pers vnw'   => 'Persoonlijk_voornaamwoord',
    'telw'       => 'Onbepaald_hoofdtelwoord',
    'te'         => 'Werkwoord',
    'vw'         => 'Voegwoord',
    'vz'         => 'Voorzetsel',
    'wederk vnw' => 'Wederkerend_voornaamwoord',
    'lw'         => 'Lidwoord',
  ]

  rev_types = Hash[ types.map { |x| [ x[1], x[0] ] } ]
  rev_types['Zelfstandig_naamwoord'] = 'de'

  type = Set.new

  doc.css('.wordlist').each do |ul|
    ul.css('li > a').each do |a|
      short = nil
      full = nil
      if m = a.inner_html.match(/^([^ ]*)\s*(?:\((.*)\))?$/)
        x, fragment =  a['href'].split(/#/)
        x = URI::encode x
        href = URI(x)
#        puts "%s %s" % [ href.to_s, fragment ]
        if (m[2])
          if (types[m[2]])
            type = types[m[2]];
            if (fragment && fragment != type)
              puts "%s: %s != %s" % [ m[1], fragment, types[m[2]] ]
            end
            fragment ||= type
            #a['href'] = "%s#%s" % [ href.to_s, fragment;
#           puts '1: %s %s' % [ m[1], fragment ]
          else
           puts "!! %s %s" % [ m[1], m[2] ]
          end
        else
          if (!fragment)
            type = find_type(href)
            if (type != fragment)
              puts "find type: %s (%s) %s" % [ m[1], type, Fragment ]
            end
            a['href'] = "%s#%s" % [ href.to_s, type.gsub(/ /, '_') ] if type;
          else
#           type = find_type(href)
            type = nil
puts "%s %s %s [ %s ]" % [ m[0],  fragment, '', rev_types[fragment] ]
            if (type)
              type = type.gsub(/ /, '_')
              if type && type != fragment
                puts "2: %s (%s) [%s] %s" % [ m[1] ,fragment, type, type != fragment ]
                a['href'] = "%s#%s" % [ href.to_s, type ];
              end
            else
                puts "3: %s (%s)" % [ m[1] ,fragment ]
                a.inner_html = "%s (%s) *" % [ m[0], rev_types[fragment] ]
            end
          end
        end
      else
        raise "bob"
      end
    end
  end
  doc
end

def test_images
  doc = nil

  File.open('./public/basiswoordenboek.html', 'r:UTF-8') do |file|
    doc = Nokogiri::XML(file);
  end

  doc.css('.wordlist').each do |ul|
    ul.css('li > button').each do |button|
      m = button['onclick'].match(/play\(this, '(.*)'[^']*$/)
      path = m[1]
      path.gsub!(/\\'/, "'")
 #    puts URI::encode('https://upload.wikimedia.org/wikipedia/commons/%s' % [ path ])
      uri = URI(URI::encode('https://upload.wikimedia.org/wikipedia/commons/%s' % [ path ]))
      Net::HTTP.start(uri.host, uri.port, :use_ssl => true ) { |http|
        response = http.head(uri.request_uri)
        if (response[ 'content-type' ] != 'application/ogg')
          puts response[ 'content-type' ]
          puts button
        end
      }
      last
    end
  end
end

def add_headers(doc)
  doc.css('.wordlist').each do |tag|
    name = tag.children[1].children[1]['id']
    name = tag.children[1].children[1].delete('id')
    tag.wrap("<div id=\"section_%s\" />\n" % [ name ]);
    tag.before('<h3 class="section_headers"><a href="#toc">%s</a></h3>' % [ name ]);
    name = tag.children[1].children[1]['id']
  end
  doc.css('.toc').children.each do |tag|
    if tag.name == 'li'
      name = tag.children[1]['href']
      name = name.match /#([A-Z])/
      tag.children[1]['href'] = "#section_%s" % [ name[1] ]
    end
  end
end

def to_list(doc)
  ret = Array.new
  doc.css('.wordlist').each do |ul|
    ul.css('li').each do |li|
      a = li.css('a').first
      ret.push Hash[
        href:  a['href'],
        name:  a.text,
        audio: li.css('button').first['onclick'].match(/play\(this, '(.*)'[^']*$/)[1],
        english: li.css('span').first.text,
      ]
    end
  end
  pp ret
end

doc = nil
File.open('./public/basiswoordenboek.html', 'r:UTF-8') do |file|
  doc = Nokogiri::XML(file);
end

case action
  when 'add-headers'
    add_headers(doc)
  when 'test-images'
    test_images(doc)
  when 'test-type'
    test_types(doc)
  when 'to-list'
    to_list(doc)
end

File.open('output.html', 'w') do |file|
  file.puts doc.to_xml
end

__END__

