# basiswoordenboek

## Basiswoordenlijst - 2000 meest gebruikte woorden


The original source for this word list is: 
[Basiswoordenlijst 2000 frequente meest woorden](http://www.dikverhaar.nl/wp-content/uploads/Basiswoordenlijst_2000_frequente_meest_woorden.pdf)

[Web page](https://gam3.gitlab.io/basiswoordenboek/basiswoordenboek.html)

[Van Dale pocketwoordenboek Nederlands als tweede taal](https://webwinkel.vandale.nl/van-dale-pocketwoordenboek-nederlands-als-tweede-taal-nt2-9789460775680)

* [ruby based anki card generator](https://github.com/rkachowski/anki-rb)

